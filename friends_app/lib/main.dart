import 'package:flutter/material.dart';


void main() => runApp(FriendsApp());

class FriendsApp extends StatelessWidget
{
  static const String _app_title =  "Meus Amigos";
  static const EdgeInsets _friend_name_padding = EdgeInsets.all(10);

  @override
  Widget build(BuildContext context) => MaterialApp(title: _app_title, home: _getHome());

  Widget _getHome()
  {
    var appBar = AppBar(title: Text(_app_title));
    var appBody = _getCenter();

    return Scaffold(appBar: appBar, body: appBody,);
  }

  Widget _getCenter()
  {
    var friendsWidgets = ["Maria", "Gabriel", "Vanessa", "Camila"]
        .map((e) => _getFriendName(e))
        .toList();
    var friendsColumn = Column(mainAxisAlignment: MainAxisAlignment.center, children: friendsWidgets,);

    return Center(child: friendsColumn,);
  }

  Widget _getFriendName(String friendName)
  {
    var friend = Text(friendName, style: TextStyle(fontSize: 50),);
    var friendDecoratedBox = DecoratedBox(decoration: BoxDecoration(color: Colors.lightBlueAccent), child: friend,);

    return Padding(padding: _friend_name_padding, child: friendDecoratedBox);
  }

}
