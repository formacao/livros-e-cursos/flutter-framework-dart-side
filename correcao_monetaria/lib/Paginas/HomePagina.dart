import 'package:correcao_monetaria/Servicos/IMoedaServico.dart';
import 'package:flutter/material.dart';

class HomePagina extends StatefulWidget
{
  final IMoedaServico _moedaServico;

  HomePagina(this._moedaServico);

  @override
  State<StatefulWidget> createState()  => _HomePaginaState();

}

class _HomePaginaState extends State<HomePagina>
{
  final TextEditingController _realController = TextEditingController();
  final TextEditingController _dolarController = TextEditingController();
  final TextEditingController _euroController = TextEditingController();

  double _dolar;
  double _euro;

  void _clearAll()
  {
    _realController.text = "";
    _dolarController.text = "";
    _euroController.text = "";
  }

  void _realChanged(String text)
  {
    if(text.isEmpty)
      {
        _clearAll();
        return;
      }

    var real = double.parse(text);
    _dolarController.text = (real/_dolar).toStringAsFixed(2);
    _euroController.text = (real/_euro).toStringAsFixed(2);
  }

  void _dolarChanged(String text)
  {
    if(text.isEmpty)
      {
        _clearAll();
        return;
      }

    var dolar = double.parse(text);
    _realController.text = (dolar * _dolar).toStringAsFixed(2);
    _euroController.text = (dolar *  _dolar/_euro).toStringAsFixed(2);
  }

  void _euroChanged(String text)
  {
    if(text.isEmpty)
      {
        _clearAll();
        return;
      }

    var euro = double.parse(text);
    _realController.text = (euro * _euro).toStringAsFixed(2);
    _dolarController.text = (euro * _euro/_dolar).toStringAsFixed(2);
  }

  @override
  Widget build(BuildContext context)
  {
    var appBar = _getBar();
    var appBody = _getBody(context);
    return Scaffold(backgroundColor: Colors.black, appBar: appBar, body: appBody,);
  }

  Widget _getBar() => AppBar(title: Text("Conversor de Moeda"), backgroundColor: Colors.green, centerTitle: true,);

  Widget _getBody(BuildContext context) => FutureBuilder<Map>(future: widget._moedaServico.getData(), builder: (context, snapshot) => _getBuilder(context, snapshot),);

  Widget _getBuilder(BuildContext context, AsyncSnapshot<Map> snapshot)
  {
    switch(snapshot.connectionState)
    {
      case ConnectionState.none:
      case ConnectionState.waiting:
      case ConnectionState.active:
        return _getTextWidget("Aguarde...");
        break;

      default:
        return _getConnectionDoneWidget(snapshot);
    }
  }

  Widget _getTextWidget(String text)
  {
    var style = TextStyle(color: Colors.green, fontSize: 25.0);
    var textWidget = Text(text, style: style, textAlign: TextAlign.center,);

    return Center(child: textWidget,);
  }

  Widget _getConnectionDoneWidget(AsyncSnapshot<Map> snapshot)
  {
    if(snapshot.hasError) return _getTextWidget("Ops! Algo deu errado.");

    _dolar = snapshot.data["results"]["currencies"]["USD"]["buy"];
    _euro = snapshot.data["results"]["currencies"]["EUR"]["buy"];

    return _getMoneyInfoWidget();
  }

  Widget _getMoneyInfoWidget()
  {
    var children = <Widget>
    [
      Icon(Icons.attach_money, size:180, color: Colors.green),
      buildTextField("Reais", "R\$ ", _realController, _realChanged),
      Divider(),
      buildTextField("Euros", "€ ", _euroController, _euroChanged),
      Divider(),
      buildTextField("Dólares", "\$ ", _dolarController, _dolarChanged)
    ];

    var column = Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: children,);

    return SingleChildScrollView(padding: EdgeInsets.all(10), child: column,);
  }

  Widget buildTextField(String label, String prefix, TextEditingController controller, void Function(String text) function)
  {
    var inputLabel = InputDecoration(labelText: label, labelStyle: TextStyle(color: Colors.green), border: OutlineInputBorder(), prefixText: prefix);
    var keyboardType = TextInputType.numberWithOptions(decimal: true);

    return TextField(controller: controller, decoration: inputLabel, style: TextStyle(fontSize: 25, color: Colors.green), onChanged: function, keyboardType: keyboardType,);
  }

}