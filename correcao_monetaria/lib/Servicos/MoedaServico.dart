import 'dart:convert';
import 'package:correcao_monetaria/Servicos/IMoedaServico.dart';
import 'package:correcao_monetaria/Servicos/IVariaveisGlobais.dart';
import 'package:http/http.dart' as http;

class MoedaServico implements IMoedaServico
{
  final IVariaveisGlobais _variaveisGlobais;

  MoedaServico(this._variaveisGlobais);

  @override
  Future<Map> getData() async
  {
    var requisicao = 	"https://api.hgbrasil.com/finance?format=json&key=${_variaveisGlobais.chaveApiHgBrasil}";
    var resposta = await http.get(requisicao);

    return json.decode(resposta.body);
  }

}