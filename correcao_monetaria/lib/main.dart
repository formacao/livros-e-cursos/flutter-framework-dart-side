import 'package:correcao_monetaria/Servicos/MoedaServico.dart';
import 'package:correcao_monetaria/Servicos/VariaveisGlobais.dart';
import 'package:flutter/material.dart';
import 'Paginas/HomePagina.dart';

void main() => runApp(CorrecaoMonetaria());

class CorrecaoMonetaria extends StatelessWidget
{
  @override
  Widget build(BuildContext context)
  {
    var tema = ThemeData(hintColor: Colors.green, primaryColor: Colors.white);
    return MaterialApp(home: HomePagina(MoedaServico(VariaveisGlobais())), theme: tema,);
  }

}

