import 'package:flutter/material.dart';
import 'package:qr_code/Pages/HomePage.dart';
import 'package:qr_code/Services/QRCodeService.dart';

void main() => runApp(QRCode());

class QRCode extends StatelessWidget
{
  static const String _app_title = "Leitor de QR Code";
  @override
  Widget build(BuildContext context) => MaterialApp(title: _app_title, theme: ThemeData(primarySwatch: Colors.red), home: HomePage(_app_title, QRCodeService()));

}
