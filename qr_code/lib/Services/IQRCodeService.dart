abstract class IQRCodeService
{
  Future<String> get qrCodeText;
}