import 'package:qr_code/Services/IQRCodeService.dart';
import 'package:qrcode_reader/qrcode_reader.dart';

class QRCodeService implements IQRCodeService
{
  @override
  Future<String> get qrCodeText => QRCodeReader().scan();
}