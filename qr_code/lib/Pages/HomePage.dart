import 'package:flutter/material.dart';
import 'package:qr_code/Services/IQRCodeService.dart';

class HomePage extends StatefulWidget
{
  final String _title;
  final IQRCodeService _qrCodeService;

  HomePage(this._title, this._qrCodeService);

  @override
  State<StatefulWidget> createState()  => _HomePageState();
}

class _HomePageState extends State<HomePage>
{

  void _captureQRCode()
  {
    widget._qrCodeService
        .qrCodeText
        .then((value) => _showDialog(value));
  }

  void _showDialog(String text)
  {
    if(text == null) text = "Nada encontrado";
    showDialog(context: context, builder: (x) => _getDialogBuilder(x, text));
  }

  AlertDialog _getDialogBuilder(BuildContext dialogContext, String text)
  {
    var dialogActions = <Widget>
    [
      FlatButton(onPressed: Navigator.of(dialogContext).pop, child: new Text("Fechar"))
    ];
    return AlertDialog(title: Text("Texto do QR Code"), content: Text(text), actions: dialogActions,);
  }

  @override
  Widget build(BuildContext context)
  {
    var appBar = AppBar(title: Text(widget._title));
    var appBody = _getBody();

    return Scaffold(appBar: appBar, body: appBody,);
  }

  Widget _getBody()
  {
    var bodyComponentes = <Widget>
    [
      RaisedButton(onPressed: _captureQRCode, child: Text("Ler QRCode"),)
    ];
    var bodyColumn = Column(mainAxisAlignment: MainAxisAlignment.center, children: bodyComponentes);

    return Center(child: bodyColumn,);
  }

}