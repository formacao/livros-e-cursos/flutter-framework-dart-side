import 'package:flutter/material.dart';

class NamedCounter extends StatefulWidget
{
  final String _name;
  NamedCounter(this._name);

  @override
  State<StatefulWidget> createState()  => _NamedCounterState();
}

class _NamedCounterState extends State<NamedCounter>
{
  int _count;
  int get count => _count;
  set count(int valor)
  {
    setState(()
    {
      _count = valor;
    });
  }

  _NamedCounterState()
  {
    _count = 0;
  }

  @override
  Widget build(BuildContext context)
  {
    var text = Text("${widget._name}: $count", style: TextStyle(fontSize: 50));
    return GestureDetector(onTap: () => count++, child: text,);
  }

}