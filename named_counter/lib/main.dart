import 'package:flutter/material.dart';
import 'package:named_counter/components/namedCounter.dart';

void main() => runApp(Contador());

class Contador extends StatelessWidget
{

  static const String _nome_aplicativo = "Contador";

  @override
  Widget build(BuildContext context)  => MaterialApp(title: _nome_aplicativo, home: Scaffold(appBar: _getAppBar(), body: _getAppBody(),));
  Widget _getAppBar() => AppBar(title: Text(_nome_aplicativo),);
  Widget _getAppBody() => Center(child: NamedCounter("Cícero"),);
}