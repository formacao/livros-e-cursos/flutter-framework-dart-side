import 'package:flutter/material.dart';

void main()
{
  Widget app = MaterialApp(title: "Flutter App", home: TutorialHome());
  runApp(app);
}

class TutorialHome extends StatelessWidget
{
  @override
  Widget build(BuildContext context)
  {
    Widget appBar = _getAppBar();
    Widget appFloatingActionButton = FloatingActionButton(tooltip: "Adicionar", child: Icon(Icons.add), onPressed: null);
    Widget appBody = Center(child: Text("Olá, mundo"));

    return Scaffold(appBar: appBar, body: appBody, floatingActionButton: appFloatingActionButton);
  }

  Widget _getAppBar()
  {
    Widget iconButton = IconButton(icon: Icon(Icons.menu), onPressed: null, tooltip: "Menu de navegação");
    Widget appTitle = Text("Exemplo de título");
    List<Widget> actionsTitleBar = <Widget>
    [
      IconButton(icon: Icon(Icons.search), onPressed: null, tooltip: "Buscar")
    ];

    return AppBar(leading: iconButton, title: appTitle, actions: actionsTitleBar,);
  }
}