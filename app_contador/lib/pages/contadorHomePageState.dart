import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'contadorHomePage.dart';

class ContadorHomePageState extends State<ContadorHomePage> {

  int _contador = 0;
  int getContador() => _contador;
  void setContador(int valor) {
    setState(() {
      _contador = valor;
    });
  }

  void _incrementarContador() => setContador(getContador() + 1);

  @override
  Widget build(BuildContext context) => Scaffold(appBar: _getAppBar(), body: _getBody(), floatingActionButton: _getFloatingActionButton(), bottomNavigationBar: _getBottomBar());

  Widget _getAppBar() {
    Widget appTitle = Text("Nosso aplicativo");
    return AppBar(title: appTitle, backgroundColor: Colors.brown);
  }

  Widget _getBody() {
    TextStyle style = Theme
        .of(context)
        .textTheme
        .headline4;

    int counterValue = getContador();
    List<Widget> textRows = <Widget>
    [
      Text("Vezes que o botão foi apertado"),
      Text("$counterValue", style: style)
    ];
    Widget mainColumn = Column(mainAxisAlignment: MainAxisAlignment.center, children: textRows);

    return Center(child: mainColumn,);
  }

  Widget _getFloatingActionButton() => FloatingActionButton(onPressed: _incrementarContador, child: Icon(Icons.add), backgroundColor: Colors.red);

  Widget _getBottomBar() {
    List<Widget> rowChildren = <Widget>
    [
      IconButton(icon: Icon(Icons.add, color: Colors.white), onPressed: _alertAddedSomething),
      IconButton(icon: Icon(Icons.add_a_photo), onPressed: _alertPhoto)
    ];

    Widget row = Row(children: rowChildren);
    Widget paddingContainer = Container(child: Padding(padding: EdgeInsets.all(20), child: row,), height: 100);

    return BottomAppBar(color: Colors.brown, child: paddingContainer,);
  }

  void _alert(String message) => print(message);
  void _alertAddedSomething() => _alert("Adicionei alguma coisa!");
  void _alertPhoto() => _alert("Adicionei uma foto");
}