import 'package:flutter/material.dart';
import 'pages/all.dart';

void main() => runApp(Contador());

class Contador extends StatelessWidget
{
  @override
  Widget build(BuildContext context) => MaterialApp(title: "Livro sobre Flutter", home: ContadorHomePage());
}